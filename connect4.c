/* ENGGEN131 Project - C Project - 2018 */
/* Connect Four */

#include "connect4.h"

/*
Name: Jack Zhang
ID: 618569743
*/
#define ARRAY_MAX_SIZE 100
#define ACSII_NUMBER_SHIFT 46

// helper function for debugging arrays by printing them
void debug_Print2DArray(int array[MAX_SIZE][MAX_SIZE], int length)
{
    for (int i = 0; i < length; i++) {
        for (int j = 0; j < length; j++) {
            printf("%d ", array[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

int SecondPlacePrize(int prize1, int prize2, int prize3)
{
    // returns the second place prize given a input of three prizes, which are positive integers.
    
    //printf("%d %d\n%d\n", prize1>=prize2 && prize1>=prize3, prize2>=prize1 && prize2>=prize3, prize1>=prize2);
    
    // find the first place prize, then find the second place prize and return it
    if (prize1>=prize2 && prize1>=prize3) {
        if (prize2 >= prize3) {
            return prize2;
        } else {
            return prize3;
        }
        
    } else if (prize2>=prize1 && prize2>=prize3) {
        if (prize1>=prize3) {
            return prize1;
        } else {
            return prize3;
        }
        
    } else {
        if (prize1<=prize2) {
            return prize2;
        } else {
            return prize1;
        }
    }
}

int FourInARow(int values[], int length)
{
    // checks a 1D array for four values which are the same, and returns the index position of where the four in a row begins
    
    int result = -1;
    for (int i = 0; i < length-3; i++) {
        // check if there are four elements in a row which are equal (connect four)
        if (values[i] == values[i+1] && values[i] == values[i+2] && values[i] == values[i+3]) {
            result = i;
            break;
        }
    }
    return result;
}

// Helper function - computes m^n
int power(int m, int n) 
{
    if (m == 0) {
        return 0;
    } else if (n == 0) {
        return 1;
    } else {
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result = result * m;
        }
        return result;
    }
}

int BinaryToDecimal(int binary)
{
    // converts a binary number input in the form of an int to a decimal output
    
    int binaryArray[ARRAY_MAX_SIZE] = {0};
    int digits = 0;
    int sum = 0;
    
    // get the number of digits in input number
    for (int i = 0; i < 255; i++) {
        if (binary/power(10, i) == 0) {
            digits = i;
            break;
        }
    }
    
    // loop through and sum the two to the powers of 
    for (int i = 0;i < digits;i++) {
        binaryArray[i] = binary % 10;
        sum += binaryArray[i]*power(2,i);
        binary /= 10;
    }
    
    return sum;
    
}

// helper function for qsort, comparison function
int arrayCompare (const void* a, const void* b)
{
    if ( *(double*)a < *(double*)b ) {
        return -1;
    } else if ( *(double*)a == *(double*)b ) {
        return 0;
    } else {
        return 1;
    }
}

double MedianAbility(double abilities[], int length)
{
    // returns the median of values in an unsorted array input of length as given in input.
    double median;
        
    // sorts the array with qsort - see arrayCompare
    qsort(abilities, length, sizeof(double), arrayCompare);
    
    // get the middle value or the mean on the middle values depending on whether there is a even or odd amount of elements in array
    if (length%2 == 1) {
        median = abilities[length/2];
    } else {
        median = (abilities[(length/2)-1] + abilities[length/2])/2.0;
    }
    
    //printf("\n");
    
    return median;
    
}

// Helper function - shifts an array to the left, but only starting from array position n
void shiftLeftFromN(char *name, int n)
{
    for (int i = n; i < ARRAY_MAX_SIZE; i++) {
        name[i] = name[i+1];
        if (i == ARRAY_MAX_SIZE-1) {
            break;
        }
    }
}

void RemoveSpaces(char *name)
{
    // remove repeated spaces in a char array
    for (int i = 0; name[i]; i++) {
        // 'deletes' repeated spaces until the next char is not a space
        while (name[i] == name[i+1] && name[i] == ' ') {
            shiftLeftFromN(name, i);
        }
    }
}

void InitialiseBoard(int board[MAX_SIZE][MAX_SIZE], int size)
{
    // initialises a MAX_SIZE-by-MAX_SIZE board of -1 then fills it with a size-by-size array of 0 starting from the top left, as well as 3(s) in the middle of the zero array
    
    // error checking
    if (size < 4 || size > 10) {
        abort();
    }
    
    // initialise a 10x10 board of -1
    for (int i = 0; i<MAX_SIZE; i++) {
        for (int j = 0; j<MAX_SIZE; j++) {
            board[i][j] = -1;
        }
    }
    
    // initalise a size-by-size board of 0
    for (int i = 0; i<size; i++) {
        for (int j = 0; j<size;j++) {
            board[i][j] = 0;
        }
    }
        
    int midpoint = size/2;
    
    // Set the midpoint(s) to 3 depending of if they are even or odd.
    if (size % 2 == 1) { //if odd, set single midpoint to 3
        board[midpoint][midpoint] = 3;
    } else { //if even, set the four midpoints to 3
        board[midpoint][midpoint] = 3;
        board[midpoint-1][midpoint] = 3;
        board[midpoint][midpoint-1] = 3;
        board[midpoint-1][midpoint-1] = 3;
    }        
}

void AddMoveToBoard(int board[MAX_SIZE][MAX_SIZE], int size, char side, int move, int player, int *lastRow, int *lastCol)
{
    // adds a move to the board. It takes the current board as input, as well as the size of the square board (number of elements in one side, the side of the intended move, the player number, and power pointers which are used to return the final position of the board. The new board is also returned with the new move placed on the board. In the case where the move chosen is invalid, nothing is changed on the board, and lastRow and lastCol output variables are set to -1 to indicate that the move was invalid.
    
    // depending on the direction, move through the board until you run out of spots or hit a non-zero element
    if (side == 'W') {
        for (int i = 0; i < size; i++) {
            //printf("move=%d, i=%d\n", move, i);
            if (board[move][i] != 0) {
                *lastRow = -1;
                *lastCol = -1;
                //printf("%d %d\n", *lastRow, *lastCol);
                return;
            } else if (board[move][i+1] != 0 || i+1 == size) {
                board[move][i] = player;
                *lastRow=move;
                *lastCol=i;
                //printf("%d %d\n", *lastRow, *lastCol);
                return;
            }
        }
    } else if (side == 'S') {
        for (int i = size-1; i >= 0; i--) {
            if (board[i][move] != 0) {
                *lastRow = -1;
                *lastCol = -1;
                //printf("%d %d\n", *lastRow, *lastCol);
                return;
            } else if (board[i-1][move] != 0 || i-1 == -1) {
                board[i][move] = player;
                *lastRow=i;
                *lastCol=move;
                //printf("%d %d\n", *lastRow, *lastCol);
                return;
            }
        }
    } else if (side == 'E') {
        for (int i = size-1; i >= 0; i--) {
            if (board[move][i] != 0) {
                *lastRow = -1;
                *lastCol = -1;
                //printf("%d %d\n", *lastRow, *lastCol);
                return;
            } else if (board[move][i-1] != 0 || i-1 == -1) {
                board[move][i] = player;
                *lastRow=move;
                *lastCol=i;
                //printf("%d %d\n", *lastRow, *lastCol);
                return;
            }
        }
    } else { // implied N side
        for (int i = 0; i < size; i++) {
            if (board[i][move] != 0) {
                *lastRow = -1;
                *lastCol = -1;
                //printf("%d %d\n", *lastRow, *lastCol);
                return;
            } else if (board[i+1][move] != 0 || i+1 == size) {
                board[i][move] = player;
                *lastRow=i;
                *lastCol=move;
                //printf("%d %d\n", *lastRow, *lastCol);
                return;
            }
        }
    }
}

// helper function to find if all the sides of a board are full, and return 1 if so
int allSidesFull(int board[MAX_SIZE][MAX_SIZE], int size) 
{
    int i = 0, j = 0;
    
    for (i = 0; i < size; i++) {
        if (board[i][j] == 0) {
            return 0;
        }
    }
    
    j = size-1;
    
    for (i = 0; i < size; i++) {
        if (board[i][j] == 0) {
            return 0;
        }
    }
    
    i = 0;
    
    for (j = 0; j < size; j++) {
        if (board[i][j] == 0) {
            return 0;
        }
    }
    
    i = size-1;
    
    for (j = 0; j < size; j++) {
        if (board[i][j] == 0) {
            return 0;
        }
    }
    
    return 1;
}

// helper function to scan the whole board for connect four
int scanAllRowsColsDiagonalsForConnectFour(int board[MAX_SIZE][MAX_SIZE], int size)
{
    //scan for connect four in rows and cols
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            
            //printf("i=%d j=%d\n", i, j);
            //printf("i=%d j=%d %d %d %d %d %d %d %d %d %d %d %d\n",i, j, i >= 0, j >= 0, i < size, j < size, i-3 >= 0, j+3 < size, board[i][j] == board[i-1][j+1], board[i][j] == board[i-2][j+2], board[i][j] == board[i-3][j+3], board[i][j] > 0, board[i][j] <= 3);
            
            //scan for connect four in cols
            if (board[i][j] == board[i+1][j] && board[i][j] == board[i+2][j] && board[i][j] == board[i+3][j] && board[i][j] > 0 && board[i][j] <= 3/* sanity check*/) {
                return 1;
            } else
            //scan for connect four in rows
            if (board[i][j] == board[i][j+1] && board[i][j] == board[i][j+2] && board[i][j] == board[i][j+3] && board[i][j] > 0 && board[i][j] <= 3/* sanity check*/) {
                return 1;
            } else
            //scan for connect four in positive diagonal (upwards slanting, like positive gradient)
            if (i >= 0 && j >= 0 && i < size && j < size && i-3 >= 0 && j+3 < size && board[i][j] == board[i-1][j+1] && board[i][j] == board[i-2][j+2] && board[i][j] == board[i-3][j+3] && board[i][j] > 0 && board[i][j] <= 3/* sanity check*/ ) {
                return 1;
            } else
            //scan for connect four in negative diagonal (downwards slanting, like negative gradient)
            if (i >= 0 && j >= 0 && i < size && j < size && i+3 < size && j+3 < size && board[i][j] == board[i+1][j+1] && board[i][j] == board[i+2][j+2] && board[i][j] == board[i+3][j+3] && board[i][j] > 0 && board[i][j] <= 3/* sanity check*/ ) {
                return 1;
            }
            
        }
    }
    return 0;
}

int CheckGameOver(int board[MAX_SIZE][MAX_SIZE], int size, int player, int row, int col)
{
    // this function checks for game over (connect four exists, or all the sides of the board are full). It returns the boolean true or false for if the game is over or not (0 = not over, 1 = over). It takes in input of the board, size of the board, the current player to check for victory, as well as the row and col value of the last move, which are unused. This function returns the current player if the game is over, and 0 if the game is not over.
    
    if (allSidesFull(board, size)) {
        return player;
    }
    
    if (scanAllRowsColsDiagonalsForConnectFour(board, size)) {
        return player;
    }
    
    return 0;
    return row + col; // stops compiler warnings about the unused variables
}

void GetDisplayBoardString(int board[MAX_SIZE][MAX_SIZE], int size, char *boardString)
{
    // this function produces the pretty string to display the board and gamestate by changing the pointer boardString. It takes in the board and the size of the board as input, essential converting it to a string with borders indicating the direction of row/col number for moves, as well as the actual board where empty spots are represented with '.', player one as 'X', player two as 'O', and middle blocker pieces as '#'.
    
    int n = 0;
    
    // loops through the board
    for (int i = 0; i <= size+3; i++) {
        for (int j = 0; j <= size+4; j++) {
            
            //printf("i=%d j=%d\n", i, j);
            
            // if conditions for each condition. E.g. the first col needs either '-' or 'W' char. In the case of the numbers, it is done just by adding the col or row number by a constant ACSII_NUMBER_SHIFT (45) which makes them into the character 0-9 accordingly
            if (j == size+4) { //make newline at end of rows
                boardString[n] = '\n';
            } else if (i == 0) { // top row
                if (j < 2 || j >= size+2) {
                    boardString[n] = '-';
                } else {
                    boardString[n] = 'N';
                }
            } else if (i == 1) { // second row
                if (j < 2 || j >= size+2) {
                    boardString[n] = '-';
                } else {
                    boardString[n] = (char)j + ACSII_NUMBER_SHIFT;
                }
            } else if (j == 0) {//first col
                if (i < 2 || i >= size+2) {
                    boardString[n] = '-';
                } else {
                    boardString[n] = 'W';
                }
            } else if (j == 1) { // second col
                if (i < 2 || i >= size+2) {
                    boardString[n] = '-';
                } else {
                    boardString[n] = (char)i + ACSII_NUMBER_SHIFT;
                }
            } else if (i == size+2) { //second to last row
                if (j < 2 || j >= size+2) {
                    boardString[n] = '-';
                } else {
                    boardString[n] = (char)j + ACSII_NUMBER_SHIFT;
                }
            } else if (i == size+3) { //last row
                if (j < 2 || j >= size+2) {
                    boardString[n] = '-';
                } else {
                    boardString[n] = 'S';
                }
            } else if (j == size+2) { // second to last col
                if (i < 2 || i >= size+2) {
                    boardString[n] = '-';
                } else {
                    boardString[n] = (char)i + ACSII_NUMBER_SHIFT;
                }
            } else if (j == size+3) { // last col
                if (i < 2 || i > size+2) {
                    boardString[n] = '-';
                } else {
                    boardString[n] = 'E';
                }
            } else { // inner connect four board
                if (board[i-2][j-2] == 0) {
                    boardString[n] = '.';
                } else if (board[i-2][j-2] == 1) {
                    boardString[n] = 'X';
                } else if (board[i-2][j-2] == 2) {
                    boardString[n] = 'O';
                } else {
                    boardString[n] = '#';
                }
            }
            
            n++;
            
            // end with null char and exit function
            if (n >= ((size+4)*(size+4))+(size+4)) { 
                boardString[n] = 0;
                return;
            }
        }
    }
}

// define a struct called move to store valid moves for bots to consider
typedef struct {
    char side;
    int pos;
    int isValid;
} Move;

// helper function to see if two boards are the same
/*
int compareBoardsAreExactlySame(int array1[MAX_SIZE][MAX_SIZE], int array2[MAX_SIZE][MAX_SIZE], int size) 
{
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            if (array1[i][j] != array2[i][j]) {
                return 0;
            }
        }
    }
    return 1;
}
*/


// helper function to copy arrays
void copy2DSquareArray(int arrayToCopyInto[MAX_SIZE][MAX_SIZE], int arrayToCopyFrom[MAX_SIZE][MAX_SIZE], int size)
{
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            arrayToCopyInto[i][j] = arrayToCopyFrom[i][j];
        }
    }
}

void GetMoveBot1(int board[MAX_SIZE][MAX_SIZE], int size, int player, char *side, int *move)
{
    /* Bot Randy
     * Randy doesn't care about what you think. Randy knows how Randy plays, but plays it anyway. It's Randy's style.
     * 
     * Randy plays randomly, but only for valid moves. If there is a winning move, then it is made.
     */
    
    Move possibleMoves[MAX_SIZE*4];
    int hypotheticalBoard[MAX_SIZE][MAX_SIZE];
    char moveChar; 
    int hypotheticalPositionRow, hypotheticalPositionCol;
    
    copy2DSquareArray(hypotheticalBoard, board, size);
    
    int n = 0;
    int numberOfValidMoves = 0;
    
    // cycle through each possible move and find:
    // 1. winning moves (do it immediately) or
    // 2. valid moves (feeds into later)
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < size; j++) {
            
            //printf("i=%d j=%d\n", i, j);
            
            // convert 0-3 to direction char
            if (i == 0) {
                moveChar = 'N';
            } else if (i == 1) {
                moveChar = 'E';
            } else if (i == 3) {
                moveChar = 'S';
            } else {
                moveChar = 'W';
            }
            
            // make a hypothetical move and evaluate it
            AddMoveToBoard(hypotheticalBoard, size, moveChar, j, player, &hypotheticalPositionRow, &hypotheticalPositionCol);
            
            // if the move wins the game, make the move immediately
            if (CheckGameOver(hypotheticalBoard, size, player, hypotheticalPositionRow, hypotheticalPositionCol)) {
                *side = moveChar;
                *move = j;
                return;
            }
            
            // if the move does not win the game, evaluate which moves are valid and which don't change the board (invalid)
            possibleMoves[n].side = moveChar;
            possibleMoves[n].pos = j;
            if (hypotheticalPositionCol == -1 || hypotheticalPositionRow == -1) {
                possibleMoves[n].isValid = 0;
            } else {
                possibleMoves[n].isValid = 1;
                numberOfValidMoves++;
            }
            n++;
            
            /*
            debug_Print2DArray(board, size);
            debug_Print2DArray(hypotheticalBoard, size);
            */
            
            // reset board
            copy2DSquareArray(hypotheticalBoard, board, size);
        }
    }
    
    int randomMove = rand() % (size*4);
    //printf("%d\n", randomMove);
    
    // loop through random moves for a valid one
    while (possibleMoves[randomMove].isValid == 0) {
        randomMove = rand() % (size*4);
    }
    
    // make the valid move
    *side = possibleMoves[randomMove].side;
    *move = possibleMoves[randomMove].pos;
    
    return;
}

void GetMoveBot2(int board[MAX_SIZE][MAX_SIZE], int size, int player, char *side, int *move)
{
    /* Bot Randy
     * Randy doesn't care about what you think. Randy knows how Randy plays, but plays it anyway. It's Randy's style.
     * 
     * Randy plays randomly, but only for valid moves. If there is a winning move, then it is made.
     */
    
    Move possibleMoves[MAX_SIZE*4];
    int hypotheticalBoard[MAX_SIZE][MAX_SIZE];
    char moveChar; 
    int hypotheticalPositionRow, hypotheticalPositionCol;
    
    copy2DSquareArray(hypotheticalBoard, board, size);
    
    int n = 0;
    int numberOfValidMoves = 0;
    
    // cycle through each possible move and find:
    // 1. winning moves (do it immediately) or
    // 2. valid moves (feeds into later)
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < size; j++) {
            
            //printf("i=%d j=%d\n", i, j);
            
            // convert 0-3 to direction char
            if (i == 0) {
                moveChar = 'N';
            } else if (i == 1) {
                moveChar = 'E';
            } else if (i == 3) {
                moveChar = 'S';
            } else {
                moveChar = 'W';
            }
            
            // make a hypothetical move and evaluate it
            AddMoveToBoard(hypotheticalBoard, size, moveChar, j, player, &hypotheticalPositionRow, &hypotheticalPositionCol);
            
            // if the move wins the game, make the move immediately
            if (CheckGameOver(hypotheticalBoard, size, player, hypotheticalPositionRow, hypotheticalPositionCol)) {
                *side = moveChar;
                *move = j;
                return;
            }
            
            // if the move does not win the game, evaluate which moves are valid and which don't change the board (invalid)
            possibleMoves[n].side = moveChar;
            possibleMoves[n].pos = j;
            if (hypotheticalPositionCol == -1 || hypotheticalPositionRow == -1) {
                possibleMoves[n].isValid = 0;
            } else {
                possibleMoves[n].isValid = 1;
                numberOfValidMoves++;
            }
            n++;
            
            /*
            debug_Print2DArray(board, size);
            debug_Print2DArray(hypotheticalBoard, size);
            */
            
            // reset board
            copy2DSquareArray(hypotheticalBoard, board, size);
        }
    }
    
    int randomMove = rand() % (size*4);
    //printf("%d\n", randomMove);
    
    // loop through random moves for a valid one
    while (possibleMoves[randomMove].isValid == 0) {
        randomMove = rand() % (size*4);
    }
    
    // make the valid move
    *side = possibleMoves[randomMove].side;
    *move = possibleMoves[randomMove].pos;
    
    return;
}

